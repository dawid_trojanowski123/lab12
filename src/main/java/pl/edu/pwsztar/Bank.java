package pl.edu.pwsztar;

import pl.edu.pwsztar.entity.Account;
import pl.edu.pwsztar.repository.AccountRepository;
import pl.edu.pwsztar.repository.impl.AccountRepositoryImpl;

import java.util.Optional;

class Bank implements BankOperation {

    private final AccountRepository accountRepository;

    Bank() {
        this.accountRepository = new AccountRepositoryImpl();
    }

    public int createAccount() {
        return accountRepository.createAccount().getNumber();
    }

    public int deleteAccount(int accountNumber) {
        Optional<Account> account = accountRepository.getAccountByNumber(accountNumber);

        return account.map(Account::getBalance).orElse(ACCOUNT_NOT_EXISTS);
    }

    public boolean deposit(int accountNumber, int amount) {
        Optional<Account> account = accountRepository.getAccountByNumber(accountNumber);
        return account.map( a -> {
            a.deposit(amount);
            return true;
        }).orElse(false);
    }

    public boolean withdraw(int accountNumber, int amount) {
        Optional<Account> account = accountRepository.getAccountByNumber(accountNumber);

        return account.map( a -> a.withDraw(amount)).orElse(false);
    }

    public boolean transfer(int fromAccount, int toAccount, int amount) {
        Optional<Account> firstAccount = accountRepository.getAccountByNumber(fromAccount);
        Optional<Account> destinationAccount = accountRepository.getAccountByNumber(toAccount);
        if (firstAccount.isPresent() && destinationAccount.isPresent()) {

            return firstAccount.get().transfer(destinationAccount.get(), amount);
        } else {
            return false;
        }
    }

    public int accountBalance(int accountNumber) {
        Optional<Account> account = accountRepository.getAccountByNumber(accountNumber);

        return account.map(Account::getBalance).orElse(ACCOUNT_NOT_EXISTS);
    }

    public int sumAccountsBalance() {
        return accountRepository.getAll()
                .stream()
                .mapToInt(Account::getBalance)
                .sum();
    }
}
