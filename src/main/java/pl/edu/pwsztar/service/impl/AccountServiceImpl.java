package pl.edu.pwsztar.repository.impl;

import pl.edu.pwsztar.entity.Account;
import pl.edu.pwsztar.repository.AccountRepository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Optional;

public class AccountRepositoryImpl implements AccountRepository {

    private static int accountCounter = 0;

    private HashMap<Integer, Account> accounts = new HashMap<>();

    public Account createAccount() {
        int number = accountCounter++;
        Account account = new Account(number, 0);
        accounts.put(number, account);
        return account;
    }

    public void deleteAccount(int accountNumber) {
        accounts.remove(accountNumber);
    }

    public Optional<Account> getAccountByNumber(int accountNumber) {
        return Optional.ofNullable(accounts.get(accountNumber));
    }

    public Collection<Account> getAll() {
        return accounts.values();
    }
}