package pl.edu.pwsztar.repository;

import pl.edu.pwsztar.entity.Account;

import java.util.Collection;
import java.util.Optional;

public interface AccountRepository {

    Account createAccount();

    Optional<Account> getAccountByNumber(int accountNumber);

    Collection<Account> getAll();

    void deleteAccount(int accountNumber);
}
