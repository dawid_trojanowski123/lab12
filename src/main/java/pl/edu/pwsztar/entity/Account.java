package pl.edu.pwsztar.entity;

public class Account {

    private final int number;
    private int balance;

    public Account(int accountNumber, int accountBalance) {
        this.number = accountNumber;
        this.balance = accountBalance;
    }

    public int getBalance() {
        return balance;
    }

    public int getNumber() {
        return number;
    }

    public void deposit(int amount) {
        balance = balance + amount;
    }

    public boolean withDraw(int amount) {
        if (amount <= balance) {
            balance = balance - amount;

            return true;
        } else {
            return false;
        }
    }

    public boolean transfer(Account targerAccount, int amount) {
        if (amount <= balance) {
            targerAccount.deposit(amount);

            return true;
        } else {
            return false;
        }
    }
}
