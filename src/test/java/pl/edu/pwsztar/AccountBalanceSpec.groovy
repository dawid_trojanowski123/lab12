package pl.edu.pwsztar

import spock.lang.Specification

class AccountBalanceSpec extends Specification {

    def "should return proper value after deposit"() {
        given: "initial data"
            def bank = new Bank()
            def accountNumber = bank.createAccount()
            def moneyToDeposit = 25
            bank.deposit(accountNumber, moneyToDeposit)
        when: "get account balance"
            def accountBalance = bank.accountBalance(accountNumber)
        then: "get proper value after deposit"
            moneyToDeposite == accountBalance
    }
}
