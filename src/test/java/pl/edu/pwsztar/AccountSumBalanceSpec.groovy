package pl.edu.pwsztar

import spock.lang.Specification

class AccountSumBalanceSpec extends Specification {

    def "should sum all accounts balance"() {
        given: "initial data"
            def bank = new Bank()
            def firstAccountNumber = bank.createAccount()
            def secondAccountNumber = bank.createAccount()
            def firstAccountMoney = 1
            def secontAccountMoney = 2
            bank.deposit(firstAccountNumber, firstAccountMoney)
            bank.deposit(secondAccountNumber, secontAccountMoney)
        when: "sums all accounts balances"
            def sumBalance = bank.sumAccountsBalance()
        then: "gets proper sum"
            sumBalance == firstAccountMoney + secontAccountMoney
    }
}
