package pl.edu.pwsztar

import spock.lang.Specification

class DeleteAccountSpec extends Specification {

    def "shouldn't delete account if account numer doesn't exit"(){
        given: "initial data"
            def bank = new Bank()
        when: "account gets deleted"
            def accountNumber = bank.deleteAccount(345)
        then: "return error"
            accountNumber == BankOperation.ACCOUNT_NOT_EXISTS
    }
}
