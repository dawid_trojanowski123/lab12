package pl.edu.pwsztar

import spock.lang.Specification

class TransferSpec extends Specification {

    def "should transfer money if account balance if bigger than tranfer value"() {
        given: "initial data"
            def bank = new Bank()
            def firstAccountNumber = bank.createAccount()
            def secondAccountNumber = bank.createAccount()
            bank.deposit(firstAccountNumber, 20)
        when: "transfer money"
            def transferResult = bank.transfer(firstAccountNumber, secondAccountNumber, 4)
        then: "transfer result completed"
            transferResult
    }

    def "shouldn't transfer money if account balance if lower than tranfer value"() {
        given: "initial data"
            def bank = new Bank()
            def firstAccountNumber = bank.createAccount()
            def secondAccountNumber = bank.createAccount()
            bank.deposit(firstAccountNumber, 20)
        when: "transfer money"
            def transferResult = bank.transfer(firstAccountNumber, secondAccountNumber, 21)
        then: "transfer result failed"
            !transferResult
    }
}
