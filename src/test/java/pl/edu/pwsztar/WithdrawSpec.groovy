package pl.edu.pwsztar

import spock.lang.Specification

class WithdrawSpec extends Specification {

    def "should withdraw money if balance of account is higher that withdraw value"() {
        given: "initial data"
            def bank = new Bank()
            def accountNumber = bank.createAccount()
            bank.deposit(accountNumber, 25)
        when: "withdraw money"
            def withdrawResult = bank.withdraw(accountNumber, 2)
        then: "withdraw result completed"
            withdrawResult
    }

    def "shouldn't withdraw money if balance of account is lower that withdraw value"() {
        given: "initial data"
            def bank = new Bank()
            def accountNumber = bank.createAccount()
        bank.deposit(accountNumber, 25)
            when: "withdraw money"
            def withdrawResult = bank.withdraw(accountNumber, 6)
        then: "withdraw result failed"
            withdrawResult
    }
}
