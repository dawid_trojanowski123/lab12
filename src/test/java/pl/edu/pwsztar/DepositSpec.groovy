package pl.edu.pwsztar

import spock.lang.Specification

class DepositSpec extends Specification {

    def "should deposit money"() {
        given: "initial data"
            def bank = new Bank()
            def accountNumber = bank.createAccount()
        when: "deposit money"
            def depositeResult = bank.deposit(accountNumber, 1)
        then: "money deposited"
            depositeResult
    }

    def "shouldn't deposit money if user account number doesn't exit"() {
        given: "initial data"
            def bank = new Bank()
        when: "deposit money"
            def depositResult = bank.deposit(43, 1)
        then: "money is not deposit"
            !depositResult
    }
}
